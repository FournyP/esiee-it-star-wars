package main;

import java.util.ArrayList;
import java.util.Map;

import modeles.Film;

public class Utils {
    
    public static void PrintFilmArray(ArrayList<Film> films) {
        for (Film film : films) {
            System.out.println(film);
        }
    }

    public static void makeBackUp(Map<String, Film> dictionary) {

        for (Map.Entry<String, Film> entry : dictionary.entrySet()) {
            System.out.println(
                    entry.getKey() + " - " + entry.getValue().getTitle() + " - " + entry.getValue().calculBenefice());
        }
    }
}
