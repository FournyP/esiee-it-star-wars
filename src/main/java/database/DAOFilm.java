package database;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import modeles.Acteur;
import modeles.Film;

public class DAOFilm extends DAOStarwars {

    /// CONST///
    private final String SQL_ADD = "INSERT INTO FILM (title, release_year, num_episode, cost, receipt, image_file) VALUES(?, ?, ?, ?, ?, ?)";
    private final String SQL_ADD_RELATION = "INSERT INTO FILM_ACTEUR (film_id, acteur_id) VALUES(?, ?)";
    private final String SQL_UPDATE = "UPDATE FILM SET title = ?, release_year = ?, num_episode = ?, cost = ?, receipt = ? imageFile = ? WHERE id = ?";
    private final String SQL_DELETE = "DELETE FROM FILM WHERE id=?";

    /// MEMBERS///
    private Statement defaultStatement;
    private PreparedStatement addStatement;
    private PreparedStatement addRelationStatement;
    private PreparedStatement updateStatement;
    private PreparedStatement deleteStatement;

    public DAOFilm(String login, String password, String url, String port, String dbName) {

        super(login, password, url, port, dbName);
        try {
            defaultStatement = conn.createStatement();
            addStatement = conn.prepareStatement(SQL_ADD);
            addRelationStatement = conn.prepareStatement(SQL_ADD_RELATION);
            updateStatement = conn.prepareStatement(SQL_UPDATE);
            deleteStatement = conn.prepareStatement(SQL_DELETE);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void close() {
        try {
            defaultStatement.close();
            addStatement.close();
            addRelationStatement.close();
            updateStatement.close();
            deleteStatement.close();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void lister() {

        List<Film> films = listerFilms();

        for (Film film : films) {
            System.out.println(film);
        }
    }

    public List<Film> listerFilms() {

        ArrayList<Film> films = new ArrayList<>();

        try {
            ResultSet result = defaultStatement.executeQuery("SELECT * FROM Film");

            while (result.next()) {
                Film film = new Film();

                film.setId(result.getInt(1));
                film.setTitle(result.getString(2));
                film.setReleaseYear(result.getInt(3));
                film.setNumEpisode(result.getInt(4));
                film.setCost(result.getInt(5));
                film.setReceipt(result.getInt(6));
                film.setImageFile(result.getBytes(7));

                films.add(film);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return films;
    }

    public Film findOneByTitle(String title) {

        Film film = null;

        try {
            ResultSet result = defaultStatement.executeQuery("SELECT * FROM Film WHERE title='" + title + "' LIMIT 1;");

            result.next();

            if (!result.wasNull()) {

                film = new Film();

                film.setId(result.getInt(1));
                film.setTitle(result.getString(2));
                film.setReleaseYear(result.getInt(3));
                film.setNumEpisode(result.getInt(4));
                film.setCost(result.getInt(5));
                film.setReceipt(result.getInt(6));
                film.setImageFile(result.getBytes(7));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return film;
    }

    public Film findOneById(int id) {

        Film film = null;

        try {
            ResultSet result = defaultStatement.executeQuery("SELECT * FROM Film WHERE id=" + id);

            result.next();

            if (!result.wasNull()) {
                film = new Film();

                film.setId(result.getInt(1));
                film.setTitle(result.getString(2));
                film.setReleaseYear(result.getInt(3));
                film.setNumEpisode(result.getInt(4));
                film.setCost(result.getInt(5));
                film.setReceipt(result.getInt(6));
                film.setImageFile(result.getBytes(7));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return film;
    }

    public void ajouter(String title, int releaseYear, int numEpisode, long cost, long receipt, byte[] imageFile,
            ArrayList<Acteur> acteurs) {

        try {

            addStatement.setString(1, title);
            addStatement.setInt(2, releaseYear);
            addStatement.setInt(3, numEpisode);
            addStatement.setLong(4, cost);
            addStatement.setLong(5, receipt);
            addStatement.setBytes(6, imageFile);

            addStatement.executeUpdate();

            Film film = findOneByTitle(title);

            for (Acteur acteur : acteurs) {
                addRelationStatement.setInt(1, film.getId());
                addRelationStatement.setInt(2, acteur.getId());

                addRelationStatement.executeUpdate();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void ajouter(Film film) {
        ajouter(film.getTitle(), film.getReleaseYear(), film.getNumEpisode(), film.getCost(), film.getReceipt(), film.getImageFile(),
                film.getActeurs());
    }

    public void supprimer(int id) {
        try {

            deleteStatement.setInt(1, id);

            boolean status = deleteStatement.execute();

            if (status) {
                System.out.println("La ligne a bien été supprimé");
            } else {
                System.out.println("La ligne n'a pas pu être supprimé");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void supprimer(Film film) {
        supprimer(film.getId());
    }

    public void modifier(int id, String title, int releaseYear, int numEpisode, long cost, long receipt, byte[] imageFile) {
        try {

            updateStatement.setString(1, title);
            updateStatement.setInt(2, releaseYear);
            updateStatement.setInt(3, numEpisode);
            updateStatement.setLong(4, cost);
            updateStatement.setLong(5, receipt);
            updateStatement.setBytes(6, imageFile);
            updateStatement.setInt(7, id);

            updateStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void modifier(Film film) {
        modifier(film.getId(), film.getTitle(), film.getReleaseYear(), film.getNumEpisode(), film.getCost(), film.getReceipt(), 
                film.getImageFile());
    }
}
