package frame;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import database.DAOFilm;

public class DeleteFrame extends JFrame implements ActionListener {
    
    private GridBagConstraints gbc;
    private JPanel mainPanel;
    private JTextField idFld;
    private JButton confirmationBtn;

    private DAOFilm daoFilm;

    public DeleteFrame() {
        gbc = new GridBagConstraints();

        daoFilm = new DAOFilm("root", "1234", "127.0.0.1", "3306", "starwars");

        initFrame();
    }

    public void initFrame() {
        this.setTitle("Films");
        initPanel();
        this.setSize(600, 500);
        this.setResizable(false);
        this.setLocationRelativeTo(null); // On centre la fenetre à l'ecran
        this.setAlwaysOnTop(false);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE); // Termine le programme si l'utilisateur clic sur la croix
        this.setVisible(true);
        this.setContentPane(mainPanel);
    }

    public void initPanel() {
        mainPanel = new JPanel();
        mainPanel.setLayout(new GridBagLayout());
        mainPanel.setVisible(true);

        // On met gdc.fill horizontal pour que les composants s'étire
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(5, 0, 5, 0);
        initField();
        initButton();
    }

    public void initField() {
        idFld = new JTextField();
        idFld.setEnabled(true);
        gbc.gridx = 0;
        gbc.gridy = 0;
        mainPanel.add(idFld, gbc);
    }

    public void initButton() {
        confirmationBtn = new JButton("Confirmer la suppression");
        confirmationBtn.setEnabled(true);
        gbc.gridx = 0;
        gbc.gridy = 1;
        mainPanel.add(confirmationBtn, gbc);
        confirmationBtn.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // TODO Auto-generated method stub
        
        String idStr = idFld.getText();

        try {

            int id = Integer.parseInt(idStr);

            daoFilm.supprimer(id);

        } catch (NumberFormatException ex) {
            ex.printStackTrace();
        } finally {
            FilmsFrame new_fenetre = new FilmsFrame();
            this.dispose();
        }
    }
}
