package frame;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

public class LoginFrame extends JFrame implements ActionListener {
    
    private GridBagConstraints gbc;
    private JPanel mainPanel;
    private JButton loginBtn;
    private JTextField usernameFld;
    private JPasswordField passwordFld;
    private JLabel usernameLbl;
    private JLabel passwordLbl;

    public LoginFrame() {
		gbc = new GridBagConstraints();
		initFrame();
	}

    public void initFrame() {
        this.setTitle("Login");
        initPanel();
        this.setSize(60, 200);
        this.setResizable(false);
        this.setLocationRelativeTo(null); // On centre la fenetre à l'ecran
        this.setAlwaysOnTop(false);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE); // Termine le programme si l'utilisateur clic sur la croix
        this.setVisible(true);
        this.setContentPane(mainPanel);
    }

    public void initPanel() {
        mainPanel = new JPanel();
        mainPanel.setLayout(new GridBagLayout());
        mainPanel.setVisible(true);

        // On met gdc.fill horizontal pour que les composants s'étire
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(3, 3, 3, 3);
        initField();
        initLabel();
        initButton();
    }

    public void initButton() {
        loginBtn = new JButton("Login");
        loginBtn.setEnabled(true);
        gbc.gridx = 0;
        gbc.gridy = 5;
        mainPanel.add(loginBtn, gbc);

        loginBtn.addActionListener(this);
    }

    public void initField() {
        usernameFld = new JTextField();
        usernameFld.setEnabled(true);
        gbc.gridx = 0;
        gbc.gridy = 1;
        mainPanel.add(usernameFld, gbc);

        passwordFld = new JPasswordField();
        passwordFld.setEnabled(true);
        gbc.gridx = 0;
        gbc.gridy = 3;
        mainPanel.add(passwordFld, gbc);
    }

    public void initLabel() {
        usernameLbl = new JLabel("Username");
        gbc.gridx = 0;
        gbc.gridy = 0;
        mainPanel.add(usernameLbl, gbc);

        passwordLbl = new JLabel("Password");
        gbc.gridx = 0;
        gbc.gridy = 2;
        mainPanel.add(passwordLbl, gbc);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == loginBtn) {
            FilmsFrame new_fenetre = new FilmsFrame();
            this.dispose();
        }
    }
}
