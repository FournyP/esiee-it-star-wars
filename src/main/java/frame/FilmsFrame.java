package frame;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.WindowConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import database.DAOFilm;
import modeles.Film;

public class FilmsFrame extends JFrame implements ActionListener {

    private GridBagConstraints gbc;
    private JPanel mainPanel;
    private JTable table;
    private JButton addBtn;
    private JButton deleteBtn;

    private DAOFilm daoFilm;

    public FilmsFrame() {
		gbc = new GridBagConstraints();

        daoFilm = new DAOFilm("root", "1234", "127.0.0.1", "3306", "starwars");

        initFrame();
    }

    public void initFrame() {
        this.setTitle("Films");
        initPanel();
        this.setSize(600, 500);
        this.setResizable(false);
        this.setLocationRelativeTo(null); // On centre la fenetre à l'ecran
        this.setAlwaysOnTop(false);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE); // Termine le programme si l'utilisateur clic sur la croix
        this.setVisible(true);
        this.setContentPane(mainPanel);
    }

    public void initPanel() {
        mainPanel = new JPanel();
        mainPanel.setLayout(new GridBagLayout());
        mainPanel.setVisible(true);

        // On met gdc.fill horizontal pour que les composants s'étire
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(5, 0, 5, 0);
        initTable();
        initButton();
    }

    public void initTable() {

        List<Film> films = daoFilm.listerFilms();
        Object[][] body = new Object[films.size()][5];

        for (int index = 0; index < films.size(); index++) {

            body[index][0] = films.get(index).getId();
            body[index][1] = films.get(index).getTitle();
            body[index][2] = films.get(index).getReleaseYear();
            body[index][3] = films.get(index).getCost();
            body[index][4] = films.get(index).getReceipt();
        }

        String[] head = { "#", "Title", "Release year", "Cost", "Receipt" };

        table = new JTable(body, head);
        table.setEnabled(true);

        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.gridwidth = 2;
        mainPanel.add(table, gbc);
        gbc.gridwidth = 1;

        table.getModel().addTableModelListener(new TableModelListener() {

            public void tableChanged(TableModelEvent e) {
                //Implement event
                String idStr = table.getValueAt(e.getFirstRow(), 0).toString();
                String title = table.getValueAt(e.getFirstRow(), 1).toString();
                String releaseYearStr = table.getValueAt(e.getFirstRow(), 2).toString();
                String costStr = table.getValueAt(e.getFirstRow(), 3).toString();
                String receiptStr = table.getValueAt(e.getFirstRow(), 4).toString();

                try {

                    int id = Integer.parseInt(idStr);
                    int releaseYear = Integer.parseInt(releaseYearStr);
                    long cost = Long.parseLong(costStr);
                    long receipt = Long.parseLong(receiptStr);

                    Film film = daoFilm.findOneById(id);

                    film.setTitle(title);
                    film.setReleaseYear(releaseYear);
                    film.setCost(cost);
                    film.setReceipt(receipt);

                    daoFilm.modifier(film);

                } catch (NumberFormatException ex) {
                    ex.printStackTrace();
                }
            }
        });
    }

    public void initButton() {
        addBtn = new JButton("Ajouter un film");
        addBtn.setEnabled(true);
        gbc.gridx = 0;
        gbc.gridy = 0;
        mainPanel.add(addBtn, gbc);
        addBtn.addActionListener(this);


        deleteBtn = new JButton("Supprimer un film");
        deleteBtn.setEnabled(true);
        gbc.gridx = 1;
        gbc.gridy = 0;
        mainPanel.add(deleteBtn, gbc);
        deleteBtn.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == addBtn) {
            FormFrame new_fenetre = new FormFrame();
            this.dispose();
        }

        if (e.getSource() == deleteBtn) {
            DeleteFrame new_fenetre = new DeleteFrame();
            this.dispose();
        }
    }
}
