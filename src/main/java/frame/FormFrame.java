package frame;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import database.DAOFilm;
import modeles.Acteur;
import modeles.Film;

public class FormFrame extends JFrame implements ActionListener {
    
    private GridBagConstraints gbc;
    private JPanel mainPanel;

    private JTextField titleFld;
    private JTextField releaseYearFld;
    private JTextField numEpisodeFld;
    private JTextField costFld;
    private JTextField receiptFld;

    private JLabel titleLbl;
    private JLabel releaseYearLbl;
    private JLabel numEpisodeLbl;
    private JLabel costLbl;
    private JLabel receiptLbl;

    private JFileChooser fileChooser;

    private JButton imageFileBtn;
    private JButton saveBtn;

    private byte[] imageFile;

    private DAOFilm daoFilm;

    public FormFrame() {
        gbc = new GridBagConstraints();

        daoFilm = new DAOFilm("root", "1234", "127.0.0.1", "3306", "starwars");

        fileChooser = new JFileChooser();

        initFrame();
    }

    public void initFrame() {
        this.setTitle("Films");
        initPanel();
        this.setSize(600, 500);
        this.setResizable(false);
        this.setLocationRelativeTo(null); // On centre la fenetre à l'ecran
        this.setAlwaysOnTop(false);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE); // Termine le programme si l'utilisateur clic sur
                                                                      // la croix
        this.setVisible(true);
        this.setContentPane(mainPanel);
    }

    public void initPanel() {
        mainPanel = new JPanel();
        mainPanel.setLayout(new GridBagLayout());
        mainPanel.setVisible(true);

        // On met gdc.fill horizontal pour que les composants s'étire
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(5, 0, 0, 0);
        initField();
        initLabel();
        initButton();
    }

    public void initField() {
        titleFld = new JTextField();
        titleFld.setEnabled(true);
        gbc.gridx = 0;
        gbc.gridy = 1;
        mainPanel.add(titleFld, gbc);

        releaseYearFld = new JTextField();
        releaseYearFld.setEnabled(true);
        gbc.gridx = 0;
        gbc.gridy = 3;
        mainPanel.add(releaseYearFld, gbc);
        
        numEpisodeFld = new JTextField();
        numEpisodeFld.setEnabled(true);
        gbc.gridx = 0;
        gbc.gridy = 5;
        mainPanel.add(numEpisodeFld, gbc);

        costFld = new JTextField();
        costFld.setEnabled(true);
        gbc.gridx = 0;
        gbc.gridy = 7;
        mainPanel.add(costFld, gbc);

        receiptFld = new JTextField();
        receiptFld.setEnabled(true);
        gbc.gridx = 0;
        gbc.gridy = 9;
        mainPanel.add(receiptFld, gbc);
    }

    public void initLabel() {
        titleLbl = new JLabel("Title");
        titleLbl.setEnabled(true);
        gbc.gridx = 0;
        gbc.gridy = 0;
        mainPanel.add(titleLbl, gbc);

        releaseYearLbl = new JLabel("Release year");
        releaseYearLbl.setEnabled(true);
        gbc.gridx = 0;
        gbc.gridy = 2;
        mainPanel.add(releaseYearLbl, gbc);

        numEpisodeLbl = new JLabel("Num episode");
        numEpisodeLbl.setEnabled(true);
        gbc.gridx = 0;
        gbc.gridy = 4;
        mainPanel.add(numEpisodeLbl, gbc);

        costLbl = new JLabel("Cost");
        costLbl.setEnabled(true);
        gbc.gridx = 0;
        gbc.gridy = 6;
        mainPanel.add(costLbl, gbc);

        receiptLbl = new JLabel("Receipt");
        receiptLbl.setEnabled(true);
        gbc.gridx = 0;
        gbc.gridy = 8;
        mainPanel.add(receiptLbl, gbc);
    }

    public void initButton() {
        imageFileBtn = new JButton("Choisir un fichier");
        imageFileBtn.setEnabled(true);
        gbc.gridx = 0;
        gbc.gridy = 11;
        mainPanel.add(imageFileBtn, gbc);
        imageFileBtn.addActionListener(this);

        saveBtn = new JButton("Ajouter un film");
        saveBtn.setEnabled(true);
        gbc.gridx = 0;
        gbc.gridy = 13;
        mainPanel.add(saveBtn, gbc);
        saveBtn.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == saveBtn) {
            String title = titleFld.getText();
            String releaseYearStr = releaseYearFld.getText();
            String numEpisodeStr = numEpisodeFld.getText();
            String costStr = costFld.getText();
            String receiptStr = receiptFld.getText();

            try {

                int numEpisode = Integer.parseInt(numEpisodeStr);
                int releaseYear = Integer.parseInt(releaseYearStr);
                long cost = Long.parseLong(costStr);
                long receipt = Long.parseLong(receiptStr);

                Film film = new Film(title, releaseYear, numEpisode, cost, receipt, new ArrayList<Acteur>());
                
                film.setImageFile(imageFile);

                daoFilm.ajouter(film);

            } catch (NumberFormatException ex) {
                ex.printStackTrace();
            } finally {
                FilmsFrame new_fenetre = new FilmsFrame();
                this.dispose();
            }
        }

        if (e.getSource() == imageFileBtn) {

            int returnVal = fileChooser.showOpenDialog(this);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fileChooser.getSelectedFile();

                try {
                    imageFile = Files.readAllBytes(file.toPath());
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
               
        }
    } 
}
