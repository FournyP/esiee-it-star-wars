package modeles;

public class Avis implements Comparable {
    
    private String commentaire;

    private byte note;

    public Avis() {
        commentaire = null;
        note = 0;
    }

    public Avis(String commentaire, byte note) {
        this.commentaire = commentaire;
        this.note = note;
    }

    public String getCommentaire() {
        return this.commentaire;
    }

    public void setCommentaire(String comm) {
        if (comm.length() > 80)
            throw new IllegalArgumentException("comm length is greater than 80");
        this.commentaire = comm;
    }

    public byte getNote() {
        return this.note;
    }

    public void setNote(byte note) {
        if (note < 0 || note > 5)
            throw new IllegalArgumentException("note have to be between 0 and 5");
        this.note = note;
    }

    @Override
    public int compareTo(Object o) {
        
        Avis a = (Avis) o;
        
        if (note > a.note)
            return 1;
        if (note < a.note)
            return -1;

        return 0;
    }
}
