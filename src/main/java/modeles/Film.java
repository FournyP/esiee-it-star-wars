package modeles;

import java.util.ArrayList;
import java.util.Collections;

public class Film {

    private int id;
    
    private String title;

    private int releaseYear;

    private int numEpisode;

    private long cost;

    private long receipt;

    private byte[] imageFile;

    private ArrayList<Acteur> acteurs;

    private ArrayList<Avis> avis;

    /**
     * Default constructor
     */
    public Film()
    {
        title = null;
        releaseYear = 0;
        numEpisode = 0;
        cost = 0;
        receipt = 0;
    }

    /**
     * Constructor of Film
     * 
     * @param title       To initialize title member
     * @param releaseYear To initialize releaseYear member
     * @param numEpisode  To initialize numEpisode member
     * @param cost        To initialize cost member
     * @param receipt     To initialize receipt member
     * @param acteurs     To initialize acteurs member
     */
    public Film(String title, int releaseYear, int numEpisode, long cost, long receipt, ArrayList<Acteur> acteurs)
    {
        this.title = title;
        this.releaseYear = releaseYear;
        this.numEpisode = numEpisode;
        this.cost = cost;
        this.receipt = receipt;
        this.acteurs = acteurs;
    }

    /**
     * Getter of id member
     * 
     * @return id
     */
    public int getId() {
        return this.id;
    }

    /**
     * Setter of id member
     * 
     * @param id New id value
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Getter of title member
     * 
     * @return title
     */
    public String getTitle() {
        return this.title;
    }

    /**
     * Setter of title member
     * 
     * @param title New title value
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Getter of releaseYear member
     * 
     * @return releaseYear
     */
    public int getReleaseYear() {
        return this.releaseYear;
    }

    /**
     * Setter of releaseYear
     * 
     * @param releaseYear New releaseYear value
     */
    public void setReleaseYear(int releaseYear) {
        this.releaseYear = releaseYear;
    }

    /**
     * Getter of numEpisode member
     * 
     * @return numEpisode
     */
    public int getNumEpisode() {
        return this.numEpisode;
    }

    /**
     * Setter of numEpisode member
     * 
     * @param numEpisode New numEpisode value
     */
    public void setNumEpisode(int numEpisode) {
        this.numEpisode = numEpisode;
    }

    /**
     * Getter of cost member
     * 
     * @return cost
     */
    public long getCost() {
        return this.cost;
    }

    /**
     * Setter of cost member
     * 
     * @param cost New cost value
     */
    public void setCost(long cost) {
        this.cost = cost;
    }

    /**
     * Getter of receipt member
     * 
     * @return receipt
     */
    public long getReceipt() {
        return this.receipt;
    }

    /**
     * Setter of receipt member
     * 
     * @param receipt New receipt value
     */
    public void setReceipt(long receipt) {
        this.receipt = receipt;
    }

    /**
     * Getter of imageFile member
     * 
     * @return receipt
     */
    public byte[] getImageFile() {
        return this.imageFile;
    }

    /**
     * Setter of imageFile member
     * 
     * @param receipt New receipt value
     */
    public void setImageFile(byte[] imageFile) {
        this.imageFile = imageFile;
    }

    /**
     * Getter of acteurs members
     * 
     * @return acteurs
     */
    public ArrayList<Acteur> getActeurs() {
        return this.acteurs;
    }

    /**
     * Setter of acteurs member
     * 
     * @param acteurs New acteurs value
     */
    public void setActeurs(ArrayList<Acteur> acteurs) {
        this.acteurs = acteurs;
    }

    /**
     * Getter of avis members
     * 
     * @return avis
     */
    public ArrayList<Avis> getAvis() {
        return this.avis;
    }

    /**
     * Setter of avis member
     * 
     * @param acteurs New avis value
     */
    public void setAvis(ArrayList<Avis> avis) {
        this.avis = avis;
    }

    @Override
    public String toString() {
        return "{" +
            " title='" + getTitle() + "'" +
            ", releaseYear='" + getReleaseYear() + "'" +
            ", numEpisode='" + getNumEpisode() + "'" +
            ", cost='" + getCost() + "'" +
            ", receipt='" + getReceipt() + "'" +
            "}";
    }

    public int nbActeurs() {
        
        if (acteurs == null)
            return 0;

        return acteurs.size();
    }

    public int nbPersonnages() {

        if (acteurs == null)
            return 0;

        int total = 0;

        for (Acteur acteur : acteurs) {
            
            if (acteur != null)
                total += acteur.nbPersonnages();
        }

        return total;
    }

    public long calculBenefice() {
        return receipt - cost;
    }

    public boolean isBefore(int annee) {
        return annee < this.releaseYear;
    }

    public void tri() {
        Collections.sort(this.acteurs);
    }

    public byte getNoteMax() {

        byte noteMax = 0;

        for (Avis avis : this.avis) {
            if (avis.getNote() > noteMax) 
                noteMax = avis.getNote();
        }

        return noteMax;
    }

    public byte getNoteMin() {

        byte noteMin = 5;

        for (Avis avis : this.avis) {
            if (avis.getNote() < noteMin) 
                noteMin = avis.getNote();
        }

        return noteMin;
    }

    public float getMoyenne() {

        int somme = 0;

        for (Avis avis : this.avis) {
            somme += avis.getNote();
        }

        return (float) somme / this.avis.size();
    }

    public byte getNoteMediane() {
        Collections.sort(this.avis);

        int medianValue = (int) ((this.avis.size() + 1) / 2);

        return this.avis.get(medianValue - 1).getNote();
    }
}
