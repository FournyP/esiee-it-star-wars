package main;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;

import modeles.Avis;
import modeles.Film;

/**
 * Unit test for simple App.
 */
public class AvisFilmTest 
{
    @Test
    public void testGetNoteMax()
    {
        Film film = new Film();
        Avis a1 = new Avis("test", (byte) 3);
        Avis a2 = new Avis("test", (byte) 4);
        ArrayList<Avis> avis = new ArrayList<Avis>();
        avis.add(a1);
        avis.add(a2);
        
        film.setAvis(avis);
        
        assertEquals((byte) 4, film.getNoteMax());
    }
    
    @Test
    public void testGetNoteMin()
    {
    	Film film = new Film();
        Avis a1 = new Avis("test", (byte) 3);
        Avis a2 = new Avis("test", (byte) 4);
        ArrayList<Avis> avis = new ArrayList<Avis>();
        avis.add(a1);
        avis.add(a2);
        
        film.setAvis(avis);
        
        assertEquals((byte) 3, film.getNoteMin());
    }
    
    @SuppressWarnings("deprecation")
	@Test
    public void testGetMoyenne()
    {
    	Film film = new Film();
        Avis a1 = new Avis("test", (byte) 3);
        Avis a2 = new Avis("test", (byte) 4);
        ArrayList<Avis> avis = new ArrayList<Avis>();
        avis.add(a1);
        avis.add(a2);
        
        film.setAvis(avis);
        
        assertEquals(3.5, film.getMoyenne(), 0);
    }
    
    @Test
    public void testGetNoteMediane()
    {
    	Film film = new Film();
        Avis a1 = new Avis("test", (byte) 3);
        Avis a2 = new Avis("test", (byte) 4);
        Avis a3 = new Avis("test", (byte) 5);
        ArrayList<Avis> avis = new ArrayList<Avis>();
        avis.add(a1);
        avis.add(a2);
        avis.add(a3);
        
        film.setAvis(avis);
        
        assertEquals((byte) 4, film.getNoteMediane());
    }
}