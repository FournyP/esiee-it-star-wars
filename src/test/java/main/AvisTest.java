package main;

import static org.junit.Assert.assertTrue;


import org.junit.Test;

import modeles.Avis;

/**
 * Unit test for simple App.
 */
public class AvisTest 
{
    @Test
    public void testSetNote()
    {
        Avis a1 = new Avis("test", (byte) 1);

        try {
            a1.setNote((byte) 100);
            
            assertTrue(false);
        } catch (IllegalArgumentException e) {
            assertTrue(true);
        }
        
        
        try {
            a1.setNote((byte) 4);
            
            assertTrue(true);
        } catch (IllegalArgumentException e) {
            assertTrue(false);
        }
    }
    
    @Test
    public void testSetCommentaire()
    {
        Avis a1 = new Avis("test", (byte) 1);

        try {
            a1.setCommentaire("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
            
            assertTrue(false);
        } catch (IllegalArgumentException e) {
            assertTrue(true);
        }
        
        
        try {
            a1.setCommentaire("aaaa");
            
            assertTrue(true);
        } catch (IllegalArgumentException e) {
            assertTrue(false);
        }
    }
}